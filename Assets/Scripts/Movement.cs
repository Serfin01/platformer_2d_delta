﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	bool saltar = false;
	public int fuerzaSalto;
	Rigidbody2D rb;
    public int speed;
    public Animator animator;
    private float horizontalMove;
    bool GirarPlayer = true;
    SpriteRenderer playerRender;
    public PlatformManager pm;
    public Vector3 checkpoint;
    public bool damage = false;
    bool isGrounded = false;
    public LayerMask Ground;
    public float checkRadius;
    public Vector3 feetOffset;
    public AudioSource audioSource;
    public AudioSource audioSource2;
    public AudioSource audioSource3;
    public AudioSource audioSource4;

    // Use this for initialization
    void Start () {
		
		rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        checkpoint = transform.position;

    }
	
	// Update is called once per frame
	void Update () {

        /*
        if (killed)
        {
            Killed();
        }
        */
       // feetOffset.y = -2 * transform.localScale.y;
        isGrounded = Physics2D.OverlapCircle(transform.position + feetOffset, checkRadius, Ground);

        Jumping();

		Movimiento();

        Attack();

        horizontalMove = Input.GetAxisRaw("Horizontal") * speed;

        animator.SetFloat("Velocity", horizontalMove);
        animator.SetFloat("velocity.Y", rb.velocity.y);

        if (!isGrounded)
        {
            animator.SetBool("Jumping", true);
        }

        if (horizontalMove > 0 && !GirarPlayer)
        {
            //girar();
            Vector3 currentScale = transform.localScale;
            currentScale.x *= -1;
            transform.localScale = currentScale;

            GirarPlayer = true;
        }
        else if (horizontalMove < 0 && GirarPlayer)
        {
            //girar();
            Vector3 currentScale = transform.localScale;
            currentScale.x *= -1;
            transform.localScale = currentScale;

            GirarPlayer = false;
        }

        
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("OnCollisionEnter2D");

        if (col.gameObject.tag == "die")
        {

            this.transform.position = checkpoint;

        }
        

        else if (col.gameObject.tag == "Moneda")
        {

            pm.AddPoints(5);

            audioSource2.Play();

            Destroy(col.gameObject);

        }

        else if (col.gameObject.tag == "Trampoline")
        {

            rb.AddForce(transform.up * 45, ForceMode2D.Impulse);

        }

    }

    void OnTriggerEnter2D(Collider2D col)
    {

            if (col.gameObject.tag == "Checkpoint")
        {

            checkpoint = col.transform.position;

        }
        else if (col.gameObject.tag == "die")
        {

            audioSource4.Play();

        }
        //    if (col.gameObject.tag == "Plataforma" && rb.velocity.y <= 0)
        //    {

        //        saltar = true;

        //        animator.SetBool("Jumping", false);

        //    }

    }

    //void OnTriggerExit2D(Collider2D col)
    //{

    //    if (col.gameObject.tag == "Plataforma")
    //    {

    //        saltar = false;

    //    }

    //}

    void Jumping(){

		if(Input.GetKeyDown(KeyCode.Space) && isGrounded && rb.velocity.y <= 0){

			rb.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);

            animator.SetBool("Jumping", true);

            audioSource.Play();

        }

        if (isGrounded)
        {

            animator.SetBool("Jumping", false);
        }
    }

	void Movimiento(){

		this.transform.Translate(Input.GetAxis("Horizontal")* speed * Time.deltaTime,0,0);

	}

    void Attack()
    {

        if (Input.GetKeyDown("e"))
        {

            animator.SetBool("melee", true);

            audioSource3.Play();


        }
        else animator.SetBool("melee", false);

    }

    void dps()
    {
        damage = true;
    }

    void girar()
    {
        GirarPlayer = !GirarPlayer;
        playerRender.flipX = !playerRender.flipX;
    }

    /*
    public void Killed()
    {
        this.transform.position = checkpoint;
    }
    */
}
