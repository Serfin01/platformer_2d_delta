﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Espada : MonoBehaviour
{
    public Movement Antonio;
    // Start is called before the first frame update
    void Start()
    {
        Antonio = GameObject.FindWithTag("Player").GetComponent<Movement>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if (Antonio.damage == true)
        {
            //Debug.Log("hola");
            if (col.gameObject.tag == "die")
            {

                Destroy(col.gameObject);
                Antonio.damage = false;

            }

        }

    }
}
