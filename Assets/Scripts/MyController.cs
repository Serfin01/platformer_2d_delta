﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
[RequireComponent(typeof(Rigidbody2D))]
public class MyController : MonoBehaviour
{
    public PlatformManager pm;

    private Rigidbody2D rb2d = null;
    public float move = 0f;
    public float maxS = 11f;
    private bool jump;

    private bool killed;
    public float fuerzaSalto = 5.0f;

    public Vector3 checkpoint;
    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        checkpoint = transform.position;
    }
    // Update is called once per frame
    
 
    private void Update()
    {
        transform.Translate(Input.GetAxis("Horizontal") * move * Time.deltaTime, 0, 0);
        

        killed = Input.GetKeyDown(KeyCode.K);
 
        if (jump)
        {
            
            rb2d.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
        }

        if(killed){
            Killed();
        }
    }




    public void Killed(){
        this.transform.position = checkpoint;
    }

    /// <summary>
    /// Sent when another object enters a trigger collider attached to this
    /// object (2D physics only).
    /// </summary>
    /// <param name="other">The other Collider2D involved in this collision.</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Checkpoint") {
            checkpoint = other.transform.position;
        } else if (other.tag == "SlowPower") {
            StartCoroutine(SlowPower());
            Destroy(other.gameObject);
        } else if (other.tag == "Moneda")
        {
            pm.AddPoints(5);
            Destroy(other.gameObject);
        }
        if (other.tag == "Plataforma")
        {
            jump = true;
        }

        IEnumerator SlowPower() {
            Time.timeScale = 0.5f;

            yield return new WaitForSeconds(2.0f);

            Time.timeScale = 1.0f;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        jump = false;
    }
}
