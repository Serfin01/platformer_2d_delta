﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void PulsaPlay(){
        Debug.LogError("He pulsado Play");

        SceneManager.LoadScene("Gameplay");
    }

    public void PulsaMenu()
    {
        Debug.LogError("He pulsado Menu");
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("menu");
    }

    public void PulsaCreditos()
    {
        Debug.LogError("He pulsado Creditos");

        SceneManager.LoadScene("creditos");
    }

    public void PulsaExit(){
        Application.Quit();
    }
}
