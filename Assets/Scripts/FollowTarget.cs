﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class FollowTarget : MonoBehaviour
{
    public Transform targetToFollow;
    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector3(targetToFollow.position.x, this.targetToFollow.position.y + 4,
        this.transform.position.z);

    }
}