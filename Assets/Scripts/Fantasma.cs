﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fantasma : MonoBehaviour
{
    [SerializeField] float rangeDistanceMin;
    [SerializeField] float rangeDistanceMid;
    [SerializeField] float rangeDistanceMax;

    [SerializeField] float rangeDistanceGhost;

    float rangeDistance = 10;
    [SerializeField] Transform player;
    [SerializeField] float velocidadEnemigo;
 
    Color colorgizmo = Color.yellow;
 
    private void Awake()
    {
        rangeDistance = rangeDistanceMid;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }
 
    private void Update()
    {
        if (Mathf.Abs(Vector2.Distance(player.position, transform.position)) < rangeDistance)
        {
            colorgizmo = Color.red;
            rangeDistance = rangeDistanceMax;

            if (Mathf.Abs(Vector2.Distance(player.position, transform.position)) > rangeDistanceGhost){
                transform.position = Vector2.MoveTowards(transform.position, player.position, Time.deltaTime * velocidadEnemigo);
            }
        }
        else
        {
            colorgizmo = Color.yellow;
            rangeDistance = rangeDistanceMid;
        }
    }
 
    private void OnDrawGizmos()
    {
        Gizmos.color = colorgizmo;
        Gizmos.DrawWireSphere(transform.position, rangeDistance);
    }
}
