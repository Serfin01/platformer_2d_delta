﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]

public class RexBehaviour : MonoBehaviour
{
    [SerializeField] Animator animator;
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float maxS = 10f;
    private bool jump;
    public int fuerzasalto;
    private bool isDead = false;
    SpriteRenderer playerRender;
    bool GirarPlayer = true;
        void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

        void FixedUpdate()
    {
        if (isDead) { return; }

        //rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
        rb2d.AddForce(Vector2.right * move * maxS);
        animator.SetFloat("Velocity", Mathf.Abs(rb2d.velocity.x));
        playerRender = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (isDead) { return; }

        move = Input.GetAxis("Horizontal");
        jump = Input.GetKeyDown(KeyCode.Space);

        if (move > 0 && !GirarPlayer)
        {
            girar();
        } else if (move <0 && GirarPlayer)
        {
            girar();
        }

        if (jump)
        {
            Debug.LogError("Salta");
            jump = false;
            rb2d.AddForce(Vector2.up * fuerzasalto, ForceMode2D.Impulse);
        }
        //animator.SetFloat("Velocity", move);
    }
    void girar()
    {
        GirarPlayer = !GirarPlayer;
        playerRender.flipX = !playerRender.flipX;
    }
    private void OnCollisionEnter2D (Collision2D collision)
    {
        Debug.LogError("Colisiono");
        if (collision.gameObject.tag == "Trap")
        {
            animator.SetTrigger("Dead");
            isDead = true;
        }
        else if (collision.gameObject.tag == "PUp1")
        {
            Debug.Log("More velocity");
            maxS *= 4;
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.tag == "Trampoline")
        {
            Debug.Log("Yuhu");
            rb2d.AddForce(Vector2.up * fuerzasalto * 2, ForceMode2D.Impulse);
        }
    }

//    public void ()
//        audio = 

}
