﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public float visionRadius;
    public float speed;
    public Patrullero harpia;

    GameObject player;

    Vector3 initalPosition;
    // Start is called before the first frame update
    void Start()
    {

        player = GameObject.FindGameObjectWithTag("Player");

        initalPosition = transform.position;

        harpia = GameObject.Find("arpia").GetComponent<Patrullero>();

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 target = initalPosition;


        float dist = Vector3.Distance(player.transform.position, transform.position);
        if (dist < visionRadius) 
        {

            harpia.enabled = false;

            target = player.transform.position;

        }

        float fixedSpeed = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target, fixedSpeed);

        Debug.DrawLine(transform.position, target, Color.green);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
    }
}
