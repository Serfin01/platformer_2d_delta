﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkpoint : MonoBehaviour
{
    public Sprite DeadBird;
    public GameObject ps;
    public AudioSource audioSource;

    //OnCollisionEnter2D(Collision2D collision)
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = DeadBird;
            ps.SetActive(true);
            audioSource.Play();
        }
    }
}
